async function findIP() {
	try {
		const response1 = await fetch('https://api.ipify.org/?format=json');
		const data1 = await response1.json();
		const ipAddress = data1.ip;
		const response2 = await fetch(`https://ip-api.com/json/${ipAddress}`);
			const data2 = await response2.json();

			const continent = data2.continent;
			const country = data2.country;
			const region = data2.regionName;
			const city = data2.city;
			const district = data2.district;

			document.querySelector('#info').innerHTML = `Continent: ${continent}<br>Country: ${country}<br>Region: ${region}<br>City: ${city}<br>District: ${district}`;
		} catch (error) {
			console.log(error);
		}
	}